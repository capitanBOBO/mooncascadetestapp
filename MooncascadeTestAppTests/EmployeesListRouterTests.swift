//
//  EmployeesListRouterTests.swift
//  MooncascadeTestAppTests
//
//  Created by Иван Савин on 20.10.2021.
//

import XCTest
import Contacts
@testable import MooncascadeTestApp

class EmployeesListRouterTests: XCTestCase {

    var router: EmployeesListRoutingLogic!
    var navigation: MockNavigation!
    var dataStore: MockDataStore!
    
    override func setUp() {
        super.setUp()
        navigation = MockNavigation()
        dataStore = MockDataStore()
        router = EmployeesListRouter(
            navigation: navigation,
            dataStore: dataStore
        )
    }
    
    func testRouteToDetails() {
        router.routeToDetails(by: IndexPath(item: 0, section: 0))
        
        XCTAssertTrue(navigation.controllerWasPushed, "Screen wasn't opened")
        XCTAssertTrue(dataStore.employeeWasRequested, "Data wasn't requested")
    }
    
    func testRouteToContact() {
        router.routeToContact(by: IndexPath(item: 0, section: 0))
        
        XCTAssertTrue(navigation.controllerWasPushed, "Screen wasn't opened")
        XCTAssertTrue(dataStore.contactWasRequested, "Contact wasn't requested")
    }
    
    func testShowError() {
        router.displayError()
        
        XCTAssertTrue(navigation.controllerWasPresented, "Error wasn't presented")
    }

}

class MockDataStore: EmployeesListDataStore {
    var employeeWasRequested: Bool = false
    var contactWasRequested: Bool = false
    
    func getEmployee(_ indexPath: IndexPath) -> Employee? {
        employeeWasRequested = true
        return Employee(
            firstName: "Alex",
            lastName: "Karu",
            contactDetails: .init(
                email: "examplealex@mail.com",
                phone: "7776238282"
            ),
            position: "IOS",
            projects: ["project2"]
        )
    }
    
    func getContact(_ indexPath: IndexPath) -> CNContact? {
        contactWasRequested = true
        let contact = CNContact()
        contact.setValue("Alex", forKey: "givenName")
        contact.setValue("Karu", forKey: "familyName")
        return contact
    }
}
