//
//  MockClasses.swift
//  MooncascadeTestAppTests
//
//  Created by Иван Савин on 20.10.2021.
//

import XCTest
import Contacts
@testable import MooncascadeTestApp

class MockContactsService: ContactsServiceLogic {
    func authorize(_ result: ((Result<ContactsAuthState, Error>) -> ())?) {
        status = .authorized
        result?(.success(status))
    }
    
    func fetchContacts() {
        let contact1 = CNContact()
        contact1.setValue("Ivan", forKey: "givenName")
        contact1.setValue("Savin", forKey: "familyName")
        
        let contact2 = CNContact()
        contact2.setValue("James", forKey: "givenName")
        contact2.setValue("Jameson", forKey: "familyName")
        
        let contact3 = CNContact()
        contact3.setValue("Alex", forKey: "givenName")
        contact3.setValue("Karu", forKey: "familyName")
        
        contacts.append(contentsOf: [contact1, contact2, contact3])
    }
    
    var contacts: [CNContact] = []
    
    var status: ContactsAuthState = .unknown
}

class MockService: EmployeeServiceLogic {
    enum MockError: Error {
        case mockError
    }
    
    var employeesTallin: [Employee] {
        return [
            Employee(
                firstName: "James",
                lastName: "Jameson",
                contactDetails: .init(
                    email: "examplejames@mail.com",
                    phone: "8889988989"
                ),
                position: "SALES",
                projects: ["project1", "project2"]
            ),
            Employee(
                firstName: "Ivan",
                lastName: "Savin",
                contactDetails: .init(
                    email: "example@mail.com",
                    phone: "9991238753"
                ),
                position: "IOS",
                projects: ["project1"]
            ),
            Employee(
                firstName: "Alex",
                lastName: "Karu",
                contactDetails: .init(
                    email: "examplealex@mail.com",
                    phone: "7776238282"
                ),
                position: "IOS",
                projects: ["project2"]
            ),
        ]
    }
    
    var employeesTartu: [Employee] {
        return [
            Employee(
                firstName: "Olga",
                lastName: "Kukk-Kaasik",
                contactDetails: .init(
                    email: "exampleolga@mail.com",
                    phone: "8889988989"
                ),
                position: "ANDROID",
                projects: ["project3"]
            ),
        ]
    }
    
    var result: Result<EmployeesResult, Error> = .failure(MockError.mockError)
    
    func getEmployees(_ source: SourceType, completion: @escaping (Result<EmployeesResult, Error>) -> ()) {
        switch source {
        case .tallin:
            result = .success(.init(employees: employeesTallin, isCached: false))
        case .tartu:
            result = .success(.init(employees: employeesTartu, isCached: false))
        }
        completion(result)
    }
}

class MockNavigation: NavigationLogic {
    var controllerWasPushed: Bool = false
    var controllerWasPresented: Bool = false
    
    func push(_ viewController: UIViewController) {
        controllerWasPushed = true
    }
    
    func present(_ viewController: UIViewController) {
        controllerWasPresented = true
    }
}
