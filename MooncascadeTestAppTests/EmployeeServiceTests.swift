//
//  EmployeeServiceTests.swift
//  MooncascadeTestAppTests
//
//  Created by Иван Савин on 21.10.2021.
//

import XCTest
@testable import MooncascadeTestApp

class EmployeeServiceTests: XCTestCase {

    var employeeService: EmployeeServiceLogic!
    var client: MockClient!

    override func setUp() {
        super.setUp()
        client = MockClient()
        employeeService = EmployeeService(client: client)
    }
    
    func prepareResponse() {
        let responseData: Data =
        """
        {
          "employees": [
            {
              "fname": "James",
              "lname": "Jameson",
              "contact_details": {
                "email": "examplejames@mail.com",
                "phone": "8889988989",
              },
              "position": "SALES",
              "projects": [
                            "project1",
                            "project2"
                        ]
            },
            {
              "fname": "Alex",
              "lname": "Karu",
              "contact_details": {
                "email": "examplealex@mail.com",
                "phone": "7776238282",
              },
              "position": "IOS",
              "projects": [
                "project2"
            ]
            },
            {
              "fname": "Ivan",
              "lname": "Savin",
              "contact_details": {
                "email": "example@mail.com"
              },
              "position": "IOS"
            }
        ] }
        """.data(using: .utf8) ?? Data()
        let response = Network.Response(data: responseData, statusCode: 200)
        let responseSource = Network.ResponseSource.online(response)
        client.response = .success(responseSource)
    }
    
    func testFetchEmployees() {
        prepareResponse()
        var checkResult: Result<EmployeesResult, Error> = .failure(Network.NetworkError.error(""))
        let expectation = XCTestExpectation(description: "Waiting response")
        employeeService.getEmployees(.tallin) { result in
            checkResult = result
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10)
        if case .success(let result) = checkResult {
            XCTAssertFalse(result.isCached, "Invalid source")
            XCTAssertFalse(result.employees.isEmpty, "Empty result data")
            XCTAssertTrue(result.employees.count == 3, "Invalid response data")
            let employee1 = result.employees.first
            XCTAssertTrue(employee1?.firstName == "James" && employee1?.lastName == "Jameson", "Invalid response data")
            let employee2 = result.employees[safe: 2]
            XCTAssertTrue(employee2?.firstName == "Ivan" && employee2?.lastName == "Savin", "Invalid response data")
            XCTAssertNil(employee2?.projects, "Invalid response data")
            XCTAssertNil(employee2?.contactDetails.phone, "Invalid response data")
        } else {
            XCTFail("Invalid result")
        }
    }
}

class MockClient: ClientInterface {
    var response: Result<Network.ResponseSource, Network.NetworkError> = .failure(.error(""))
    
    func sendRequest(_ request: Network.Request, completion: @escaping (Result<Network.ResponseSource, Network.NetworkError>) -> ()) {
        completion(response)
    }
}
