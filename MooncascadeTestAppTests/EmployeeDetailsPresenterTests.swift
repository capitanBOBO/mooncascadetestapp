//
//  EmployeeDetailsPresenterTests.swift
//  MooncascadeTestAppTests
//
//  Created by Иван Савин on 20.10.2021.
//

import XCTest
@testable import MooncascadeTestApp

class EmployeeDetailsPresenterTests: XCTestCase {

    var presenter: (EmployeeDetailsBusinessLogic & EmployeeDetailsDataStore)!
    var view: DetailsMockView!
    var contactsService: MockContactsService!
    
    override func setUp() {
        super.setUp()
        view = DetailsMockView()
        contactsService = MockContactsService()
        let employee = Employee(
            firstName: "Alex",
            lastName: "Karu",
            contactDetails: .init(
                email: "examplealex@mail.com",
                phone: "7776238282"
            ),
            position: "IOS",
            projects: ["project2"]
        )
        presenter = EmployeeDetailsPresenter(
            view: view,
            contactsService: contactsService,
            employee: employee
        )
    }

    func testFetchData() {
        presenter.fetchData()
        
        XCTAssertTrue(view.viewModel.fullName == "Alex Karu", "Invalid viewModel")
        XCTAssertTrue(contactsService.status == .authorized, "Invalid contacts service auth status")
        XCTAssertTrue(contactsService.contacts.count == 3, "Invalid contacts count")
    }
    
    func testGetContacts() {
        presenter.fetchData()
        
        let contact = presenter.getContact()
        XCTAssertNotNil(contact, "Contact not found")
        XCTAssertTrue(contact?.givenName == "Alex", "Invalid contact")
    }
}

class DetailsMockView: EmployeeDetailsViewLogic {
    var viewModel: EmployeeDetailsScreen.ViewModel = .init(fullName: "", email: "", phone: "", position: "", projects: [], hasContact: false)
    
    func applyViewModel(_ viewModel: EmployeeDetailsScreen.ViewModel) {
        self.viewModel = viewModel
    }
}
