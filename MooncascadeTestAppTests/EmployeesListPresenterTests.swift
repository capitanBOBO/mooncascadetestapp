//
//  EmployeesListPresenterTests.swift
//  MooncascadeTestAppTests
//
//  Created by Иван Савин on 20.10.2021.
//

import XCTest
@testable import MooncascadeTestApp
import Contacts

class EmployeesListPresenterTests: XCTestCase {

    var presenter: (EmployeesListBusinessLogic & EmployeesListDataStore)!
    var service: MockService!
    var view: ListMockView!
    var contactsService: MockContactsService!
    
    override func setUp() {
        super.setUp()
        service = MockService()
        view = ListMockView()
        contactsService = MockContactsService()
        presenter = EmployeesListPresenter(
            sourceType: .tallin,
            view: view,
            service: service,
            contactsService: contactsService
        )
    }
    
    func testFetchEmployees() {
        
        presenter.fetchData()
        
        XCTAssertTrue(view.viewModel.sectionViewModels.count == 2, "Invalid sectionViewModels count")
        let section = view.viewModel.sectionViewModels[safe: 0]
        XCTAssertTrue(section?.title == "IOS", "Invalid sections order")
        XCTAssertTrue(section?.cellViewModels.count == 2, "Invalid cellViewModels count")
        
        let cell = section?.cellViewModels[safe: 0]
        XCTAssertTrue(cell?.title == "Alex Karu", "Invalid cells order")
        
        XCTAssertFalse(view.viewModel.isCachedData, "Data cache status was changed")
        XCTAssertFalse(view.errorWasDisplayed, "Error was displayed")
        
        XCTAssertTrue(contactsService.status == .authorized, "Invalid contacts service auth status")
        XCTAssertTrue(self.contactsService.contacts.count == 3, "Invalid contacts count")
    }
    
    func testChangeSource() {
        
        presenter.fetchData()
        presenter.sourceWasChanged(.tartu)
        
        XCTAssertTrue(view.viewModel.sectionViewModels.count == 1, "Invalid sectionViewModels count")
        let section = view.viewModel.sectionViewModels[safe: 0]
        XCTAssertTrue(section?.title == "ANDROID", "Invalid sections order")
        XCTAssertTrue(section?.cellViewModels.count == 1, "Invalid cellViewModels count")
        
        let cell = section?.cellViewModels[safe: 0]
        XCTAssertTrue(cell?.title == "Olga Kukk-Kaasik", "Invalid cells order")
        
        XCTAssertFalse(view.viewModel.isCachedData, "Data cache status was changed")
        XCTAssertFalse(view.errorWasDisplayed, "Error was displayed")
    }
    
    func testSearchEmployees() {
        
        presenter.fetchData()
        presenter.search(searchText: "Ivan")
        
        XCTAssertFalse(view.errorWasDisplayed, "Error was displayed")
        
        let section = view.viewModel.sectionViewModels[safe: 0]
        XCTAssertTrue(section?.cellViewModels.count == 1, "Incorrect sectionViewModel")
        XCTAssertTrue(section?.title == "IOS", "Incorrect sectionViewModel")
        
        let cell = section?.cellViewModels[safe: 0]
        XCTAssertTrue(cell?.title == "Ivan Savin", "Incorrect cellViewModel")
    }
    
    func testGetEmployee() {
        
        presenter.fetchData()
        
        let employee = presenter.getEmployee(IndexPath(item: 0, section: 0))
        XCTAssertNotNil(employee, "Employee not found")
        XCTAssertTrue(employee?.firstName == "Alex", "Invalid employee")
    }
    
    func testGetContact() {
        
        presenter.fetchData()
        
        let contact = presenter.getContact(IndexPath(item: 0, section: 0))
        XCTAssertNotNil(contact, "Contact not found")
        XCTAssertTrue(contact?.givenName == "Alex", "Invalid contact")
    }
}

class ListMockView: EmployeesListViewLogic {
    var viewModel: EmployeesListScreen.ViewModel = .init(sectionViewModels: [], isCachedData: false)
    var errorWasDisplayed: Bool = false
    
    func applyViewModel(_ viewModel: EmployeesListScreen.ViewModel) {
        self.viewModel = viewModel
    }
    
    func displayError() {
        errorWasDisplayed = true
    }
}


