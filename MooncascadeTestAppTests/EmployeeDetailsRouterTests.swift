//
//  EmployeeDetailsRouterTests.swift
//  MooncascadeTestAppTests
//
//  Created by Иван Савин on 21.10.2021.
//

import XCTest
import Contacts
@testable import MooncascadeTestApp

class EmployeeDetailsRouterTests: XCTestCase {

    var router: EmployeeDetailsRoutingLogic!
    var dataStore: DetailsMockDataSotre!
    var navigation: MockNavigation!
    
    override func setUp() {
        super.setUp()
        dataStore = DetailsMockDataSotre()
        navigation = MockNavigation()
        router = EmployeeDetailsRouter(
            navigation: navigation,
            dataStore: dataStore
        )
    }
    
    func testRouteToContact() {
        router.routeToContact()
        
        XCTAssertTrue(dataStore.contactWasRequested, "Data wasn't requested")
        XCTAssertTrue(navigation.controllerWasPushed, "Screen wasn't opened")
    }
    
}

class DetailsMockDataSotre: EmployeeDetailsDataStore {
    var contactWasRequested = false
    
    func getContact() -> CNContact? {
        contactWasRequested = true
        let contact = CNContact()
        contact.setValue("Alex", forKey: "givenName")
        contact.setValue("Karu", forKey: "familyName")
        return contact
    }
}
