//
//  CachedNetworkClient.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 19.10.2021.
//

import Foundation

final class CachedNetworkClient: ClientInterface {
    
    private let client: ClientInterface
    private let cacheService: CacheServiceLogic
    
    init(client: ClientInterface, cacheService: CacheServiceLogic) {
        self.client = client
        self.cacheService = cacheService
    }
    
    func sendRequest(_ request: Network.Request, completion: @escaping (Result<Network.ResponseSource, Network.NetworkError>) -> ()) {
        if let responseData = cacheService.load(for: request) {
            let response = Network.Response(data: responseData, statusCode: 200)
            let source = Network.ResponseSource.cache(response)
            DispatchQueue.main.async {
                completion(.success(source))
            }
        }
        client.sendRequest(request) { [weak self] result in
            guard let self = self else { return }
            if case .success(let source) = result,
               source.response.statusCode == 200 {
                self.cacheService.save(data: source.response.data, for: request)
            }
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    
}
