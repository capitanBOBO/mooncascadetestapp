//
//  NetworkService.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 10.10.2021.
//

import Foundation

enum Network {
    enum RequestMethod {
        case get
    }

    struct Request {
        var method: RequestMethod
        var url: String
    }

    struct Response {
        var data: Data
        var statusCode: Int
    }
    
    enum ResponseSource {
        case cache(Response)
        case online(Response)
        
        var response: Response {
            switch self {
            case .cache(let response):
                return response
            case .online(let response):
                return response
            }
        }
        
        var isCache: Bool {
            if case .cache = self {
                return true
            }
            return false
        }
    }

    enum NetworkError: Error {
        case invalidUrl(_ url: String)
        case invalidStatusCode(_ statusCode: Int)
        case invalidData
        case cancelled
        case error(String)
        
        var localizedDescription: String {
            switch self {
            case .invalidUrl(let url):
                return "Invalid url \(url)"
            case .invalidStatusCode(let statusCode):
                return "Invalid status code \(statusCode)"
            case .invalidData:
                return "Invalid data"
            case .cancelled:
                return "Resquest was canceled"
            case .error(let string):
                return string
            }
        }
    }

}

protocol ClientInterface: AnyObject {
    func sendRequest(_ request: Network.Request, completion: @escaping (Result<Network.ResponseSource, Network.NetworkError>)->())
}

class NetworkClient: ClientInterface {
    
    static let shared = NetworkClient()
    
    private let session = URLSession(configuration: .default)
    private var dataTask: URLSessionDataTask?
    
    func sendRequest(_ request: Network.Request, completion: @escaping (Result<Network.ResponseSource, Network.NetworkError>)->()) {
        guard let url = URL(string: request.url) else {
            completion(.failure(Network.NetworkError.invalidUrl(request.url)))
            return
        }
        let urlRequest = URLRequest(url: url)
        dataTask?.cancel()
        dataTask = session.dataTask(with: urlRequest) { data, response, error in
            guard error == nil else {
                if let urlError = error as? URLError,
                   urlError.errorCode == -999 {
                    completion(.failure(.cancelled))
                } else {
                    completion(.failure(.error(error?.localizedDescription ?? "")))
                }
                return
            }
            guard let data = data else {
                completion(.failure(.invalidData))
                return
            }
            let statusCode = (response as? HTTPURLResponse)?.statusCode ?? 500
            guard case 200...399 = statusCode else {
                completion(.failure(.invalidStatusCode(statusCode)))
                return
            }
            let sourceResponse = Network.Response(data: data, statusCode: statusCode)
            let source = Network.ResponseSource.online(sourceResponse)
            completion(.success(source))
        }
        dataTask?.resume()
    }
}
