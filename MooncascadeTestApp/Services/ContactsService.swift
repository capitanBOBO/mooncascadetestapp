//
//  ContactsService.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 17.10.2021.
//

import Foundation
import Contacts
import ContactsUI

struct Contact {
    var fullName: String {
        return "\(contact.givenName) \(contact.familyName)"
    }
    private let contact: CNContact
    
    init(contact: CNContact) {
        self.contact = contact
    }
}

enum ContactsAuthState {
    case unknown
    case unauthorized
    case authorized
    
    init(for status: CNAuthorizationStatus) {
        switch status {
        case .notDetermined:
            self = .unknown
        case .restricted, .denied:
            self = .unauthorized
        case .authorized:
            self = .authorized
        default:
            self = .unknown
        }
    }
}

protocol ContactsServiceLogic: AnyObject {
    func authorize(_ result: ((Result<ContactsAuthState, Error>)->())?)
    func fetchContacts()
    var contacts: [CNContact] { get }
    var status: ContactsAuthState { get }
}

final class ContactsService: ContactsServiceLogic {
    
    static let shared = ContactsService()
    
    var contacts: [CNContact] = []
    var status: ContactsAuthState {
        .init(for: CNContactStore.authorizationStatus(for: .contacts))
    }
    
    func authorize(_ result: ((Result<ContactsAuthState, Error>)->())?) {
        CNContactStore().requestAccess(for: .contacts) { access, error in
            if let error = error {
                result?(.failure(error))
                return
            }
            if access {
                result?(.success(.authorized))
            } else {
                result?(.success(.unauthorized))
            }
        }
    }
    
    func fetchContacts() {
        let keys = [CNContactViewController.descriptorForRequiredKeys()]
        let request = CNContactFetchRequest(keysToFetch: keys)
        contacts.removeAll()
        do {
            try CNContactStore().enumerateContacts(with: request) { [weak self] contact, _ in
                guard let self = self else { return }
                self.contacts.append(contact)
            }
        } catch {
            print(error)
        }
    }
}
