//
//  CacheService.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 19.10.2021.
//

import Foundation

protocol CacheServiceLogic {
    func save(data: Data, for request: Network.Request)
    func load(for request: Network.Request) -> Data?
}

final class CacheService: CacheServiceLogic {
    var basePath: URL? {
        try? FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    }
    
    func save(data: Data, for request: Network.Request) {
        guard case .get = request.method else { return }
        let cacheKey = "cache" + request.url.utf8.map { String($0) }.joined()
        guard let fullPath = basePath?.appendingPathComponent(cacheKey) else { return }
        _ = try? data.write(to: fullPath)
    }
    
    func load(for request: Network.Request) -> Data? {
        guard case .get = request.method else { return nil }
        let cacheKey = "cache" + request.url.utf8.map { String($0) }.joined()
        guard let fullPath = basePath?.appendingPathComponent(cacheKey) else { return nil }
        return try? Data(contentsOf: fullPath)
    }
}
