//
//  EmployeeEndpoint.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 10.10.2021.
//

import Foundation

enum EmployeeEndpoint {
    case tallin
    case tartu
    
    var method: Network.RequestMethod {
        return .get
    }
    
    var path: String {
        switch self {
        case .tartu:
            return "https://tartu-jobapp.aw.ee/employee_list"
        case .tallin:
            return "https://tallinn-jobapp.aw.ee/employee_list"
        }
    }
}
