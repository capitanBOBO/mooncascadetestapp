//
//  EmployeeServices.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 03.10.2021.
//

import Foundation

enum SourceType: Int {
    case tallin
    case tartu
    
    var endpoint: EmployeeEndpoint {
        switch self {
        case .tallin:
            return .tallin
        case .tartu:
            return .tartu
        }
    }
}


protocol EmployeeServiceLogic: AnyObject {
    func getEmployees(_ source: SourceType, completion: @escaping (Result<EmployeesResult, Error>)->())
}

struct EmployeeResponse: Codable {
    let employees: [Employee]
}

struct EmployeesResult {
    let employees: [Employee]
    let isCached: Bool
}

final class EmployeeService: EmployeeServiceLogic {
    
    var client: ClientInterface
    
    init(client: ClientInterface) {
        self.client = client
    }
    
    func getEmployees(_ source: SourceType, completion: @escaping (Result<EmployeesResult, Error>)->()) {
        let request = Network.Request(method: source.endpoint.method, url: source.endpoint.path)
        client.sendRequest(request) { result in
            var completionResult: Result<EmployeesResult, Error>
            switch result {
            case .success(let source):
                do {
                    let employeesResponse = try JSONDecoder().decode(EmployeeResponse.self, from: source.response.data)
                    let employeesResult = EmployeesResult(employees: employeesResponse.employees, isCached: source.isCache)
                    completionResult = .success(employeesResult)
                } catch {
                    completionResult = .failure(error)
                }
            case .failure(let error):
                if case .cancelled = error {
                    print("cancelled")
                    return
                } else {
                    completionResult = .failure(error)
                }
            }
            DispatchQueue.main.async {
                completion(completionResult)
            }
        }
    }
}
