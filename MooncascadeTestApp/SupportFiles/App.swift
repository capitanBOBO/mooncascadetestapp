//
//  App.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 20.10.2021.
//

import Foundation

final class App {
    static let app = App()
    
    private var cache: CacheServiceLogic = CacheService()
    private var networkClient: ClientInterface = NetworkClient()
    
    var defaultClient: ClientInterface {
        return CachedNetworkClient(client: networkClient, cacheService: cache)
    }
}
