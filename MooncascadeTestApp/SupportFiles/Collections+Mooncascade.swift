//
//  Collections+Mooncascade.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 10.10.2021.
//

import Foundation

extension Collection {
    subscript(safe index: Index) -> Element? {
        return self.indices.contains(index) ? self[index] : nil
    }
}
