//
//  MagicLayout.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 08.10.2021.
//

import UIKit
@discardableResult
func ~= <T>(lsh: NSLayoutAnchor<T>, rsh: NSLayoutAnchor<T>) -> NSLayoutConstraint {
    let constraint = lsh.constraint(equalTo: rsh)
    constraint.isActive = true
    return constraint
}

@discardableResult
func ~= <T>(lhs: NSLayoutAnchor<T>, rhs: (anchor: NSLayoutAnchor<T>, const: CGFloat)) -> NSLayoutConstraint {
    let constraint = lhs.constraint(equalTo: rhs.anchor, constant: rhs.const)
    constraint.isActive = true
    return constraint
}

func ~= (lhs: NSLayoutDimension, rhs: CGFloat) {
    lhs.constraint(equalToConstant: rhs).isActive = true
}

func >= (lhs: NSLayoutDimension, rhs: CGFloat) {
    lhs.constraint(greaterThanOrEqualToConstant: rhs).isActive = true
}

@discardableResult
func + <T>(lsh: NSLayoutAnchor<T>, rsh: CGFloat) -> (anchor: NSLayoutAnchor<T>, const: CGFloat) {
    return (anchor: lsh, const: rsh)
}

@discardableResult
func - <T>(lsh: NSLayoutAnchor<T>, rsh: CGFloat) -> (anchor: NSLayoutAnchor<T>, const: CGFloat) {
    return (anchor: lsh, const: -rsh)
}

extension UIView {
    enum Side {
        case left
        case right
        case top
        case bottom
    }

    func prepareForAutoLayout() -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        return self
    }
    
    func pinEdgesToSuperviewEdges(top: CGFloat? = nil, left: CGFloat? = nil, bottom: CGFloat? = nil, right: CGFloat? = nil) {
        guard let superview = self.superview else { fatalError("Not in superview") }
        if let top = top {
            topAnchor ~= superview.topAnchor + top
        }
        if let bottom = bottom {
            bottomAnchor ~= superview.bottomAnchor - bottom
        }
        if let right = right {
            trailingAnchor ~= superview.trailingAnchor - right
        }
        if let left = left {
            leadingAnchor ~= superview.leadingAnchor + left
        }
    }
    
    func pinEdgesToSuperviewEdges() {
        pinEdgesToSuperviewEdges(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func pinEdgesToSuperviewEdges(excluding sides: [Side], offset: CGFloat = 0) {
        sides.forEach {
            switch $0 {
            case .top:
                pinEdgesToSuperviewEdges(left: offset, bottom: offset, right: offset)
            case .left:
                pinEdgesToSuperviewEdges(top: offset, bottom: offset, right: offset)
            case .bottom:
                pinEdgesToSuperviewEdges(top: offset, left: offset, right: offset)
            case .right:
                pinEdgesToSuperviewEdges(top: offset, left: offset, bottom: offset)
            }
        }
    }
    
    func pinCenterToSuperviewCenter() {
        guard let superview = superview else { fatalError("Not in superview") }
        centerXAnchor ~= superview.centerXAnchor
        centerYAnchor ~= superview.centerYAnchor
    }
}
