//
//  EmployeesListPresenter.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 03.10.2021.
//

import Foundation
import Contacts

protocol EmployeesListBusinessLogic: AnyObject {
    func fetchData()
    func sourceWasChanged(_ source: SourceType)
    func search(searchText: String)
}

protocol EmployeesListDataStore: AnyObject {
    func getEmployee(_ indexPath: IndexPath) -> Employee?
    func getContact(_ indexPath: IndexPath) -> CNContact?
}

final class EmployeesListPresenter {
    private var sourceType: SourceType
    private var storedEmployees: [[Employee]] = []
    private var searchedEmployees: [[Employee]] = []
    private weak var view: EmployeesListViewLogic?
    private var service: EmployeeServiceLogic
    private var contactsService: ContactsServiceLogic
    private var searchInProgress: Bool = false
    private var searchText: String = ""
    var contact: CNContact?
    
    init(
        sourceType: SourceType,
        view: EmployeesListViewLogic,
        service: EmployeeServiceLogic,
        contactsService: ContactsServiceLogic
    ) {
        self.sourceType = sourceType
        self.view = view
        self.service = service
        self.contactsService = contactsService
    }
}

//MARK: Logic

extension EmployeesListPresenter: EmployeesListBusinessLogic {
    func fetchData() {
        contactsAuth()
        service.getEmployees(sourceType) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let employeesResult):
                self.storedEmployees.removeAll()
                self.storedEmployees = self.sortEmployees(employeesResult.employees)
                if self.searchInProgress {
                    self.search(self.searchText, forCachedData: employeesResult.isCached)
                } else {
                    self.prepareViewModel(self.storedEmployees, isCachedData: employeesResult.isCached)
                }
            case .failure(let error):
                self.view?.displayError()
                print(error)
            }
        }
    }
    
    func sourceWasChanged(_ source: SourceType) {
        sourceType = source
        fetchData()
    }
    
    func search(searchText: String) {
        search(searchText, forCachedData: false)
    }
}

//MARK: DataStore

extension EmployeesListPresenter: EmployeesListDataStore {
    func getEmployee(_ indexPath: IndexPath) -> Employee? {
        let tmpEmployees = searchInProgress ? searchedEmployees : storedEmployees
        guard let section = tmpEmployees[safe: indexPath.section],
              let employee = section[safe: indexPath.item]
        else { return nil }
        return employee
    }
    
    func getContact(_ indexPath: IndexPath) -> CNContact? {
        let tmpEmployees = searchInProgress ? searchedEmployees : storedEmployees
        guard let section = tmpEmployees[safe: indexPath.section],
              let employee = section[safe: indexPath.item]
        else { return nil }
        let name = "\(employee.firstName) \(employee.lastName)"
        let contact = contactsService.contacts.first(where: {
            $0.givenName == name ||
            "\($0.givenName) \($0.familyName)" == name
        })
        return contact
    }
}

//MARK: Private

private extension EmployeesListPresenter {
    func search(_ searchText: String, forCachedData: Bool) {
        guard !searchText.isEmpty else {
            self.searchText = ""
            searchInProgress = false
            prepareViewModel(storedEmployees)
            return
        }
        self.searchText = searchText.lowercased()
        searchInProgress = true
        searchedEmployees.removeAll()
        var employees: [Employee] = []
        employees = storedEmployees.flatMap { $0 }.filter { employee in
            (employee.firstName.lowercased().contains(self.searchText) ||
            employee.lastName.lowercased().contains(self.searchText) ||
            employee.contactDetails.email.lowercased().contains(self.searchText) ||
            employee.position.lowercased().contains(self.searchText) ||
            employee.projects?.first(where: { $0.lowercased().contains(self.searchText) }) != nil) &&
            !employees.contains(where: { employee.firstName+employee.lastName == $0.firstName+$0.lastName })
        }
        searchedEmployees = sortEmployees(employees)
        prepareViewModel(searchedEmployees, isCachedData: forCachedData)
    }
    
    func contactsAuth() {
        switch contactsService.status {
        case .unknown:
            contactsService.authorize { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let status):
                    if status == .authorized {
                        self.fetchContacts()
                    }
                case .failure(let error):
                    print(error)
                }
            }
        case .authorized:
            fetchContacts()
        default:
            break
        }
    }
    
    func fetchContacts() {
        guard contactsService.contacts.isEmpty else { return }
        contactsService.fetchContacts()
        if !storedEmployees.isEmpty {
            prepareViewModel(storedEmployees)
        }
    }
    
    func sortEmployees(_ employees: [Employee]) -> [[Employee]] {
        var retEmployees: [[Employee]] = []
        employees.sorted(by: { $0.position < $1.position }).forEach { employee in
            if !retEmployees.contains(where: { $0.first?.position == employee.position }) {
                let tmp: [Employee] = employees.filter({ $0.position == employee.position })
                retEmployees.append(tmp.sorted(by: { $0.lastName < $1.lastName }))
            }
        }
        return retEmployees
    }
    
    func prepareViewModel(_ sortedEmployees: [[Employee]], isCachedData: Bool = false) {
        var sections: [EmployeesListScreen.SectionViewModel] = []
        sortedEmployees.forEach { employees in
            let cellModels: [EmployeesListCell.Model] = employees.map { employee in
                let title = "\(employee.firstName) \(employee.lastName)"
                let hasContact = contactsService.contacts.first(where: {
                    "\($0.givenName) \($0.familyName)".lowercased() == title.lowercased() ||
                    $0.givenName.lowercased() == title.lowercased()
                }) != nil
                return .init(title: title, showContactButton: hasContact)
            }
            let section = EmployeesListScreen.SectionViewModel(
                title: employees.first?.position ?? "",
                cellViewModels: cellModels
            )
            sections.append(section)
        }
        view?.applyViewModel(
            EmployeesListScreen.ViewModel(sectionViewModels: sections, isCachedData: isCachedData)
        )
    }
}
