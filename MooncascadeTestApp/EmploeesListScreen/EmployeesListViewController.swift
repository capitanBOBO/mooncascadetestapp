//
//  EmployeesListViewController.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 03.10.2021.
//

import UIKit

protocol NavigationLogic: AnyObject {
    func push(_ viewController: UIViewController)
    func present(_ viewController: UIViewController)
}

protocol EmployeesListViewLogic: AnyObject {
    func applyViewModel(_ viewModel: EmployeesListScreen.ViewModel)
    func displayError()
}

final class EmployeesListViewController: UIViewController {
    
    //MARK: Typealiases
    typealias SectionModel = EmployeesListScreen.SectionViewModel
    typealias CellModel = EmployeesListCell.Model
    
    //MARK: Properties
    private var dataSource: UICollectionViewDiffableDataSource<SectionModel, CellModel>?
    var presenter: EmployeesListBusinessLogic?
    var router: EmployeesListRoutingLogic?
    private let loader = UIActivityIndicatorView(style: .large).prepareForAutoLayout()
    private let refreshControll = UIRefreshControl()
    
    private let updateLabelView: UIView = {
        let view = UIView().prepareForAutoLayout()
        view.backgroundColor = .systemFill
        view.layer.cornerRadius = 8
        view.isHidden = true
        
        let loader = UIActivityIndicatorView(style: .medium).prepareForAutoLayout()
        view.addSubview(loader)
        loader.pinEdgesToSuperviewEdges(top: 8, left: 8, bottom: 8)
        loader.startAnimating()
        
        let label = UILabel().prepareForAutoLayout()
        label.text = "Updating..."
        label.textAlignment = .center
        view.addSubview(label)
        label.pinEdgesToSuperviewEdges(top: 8, bottom: 8, right: 8)
        label.leadingAnchor ~= loader.trailingAnchor + 8
        return view
    }()
    
    private let segmentedControl: UISegmentedControl = {
        let control = UISegmentedControl().prepareForAutoLayout()
        control.insertSegment(withTitle: "Tallin", at: 0, animated: false)
        control.insertSegment(withTitle: "Tartu", at: 1, animated: false)
        control.selectedSegmentIndex = 0
        return control
    }()
    
    private let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.placeholder = "Search"
        return searchBar
    }()
    
    private var collectionView: UICollectionView = {
        let layout = UICollectionViewCompositionalLayout { section, env in
            var config = UICollectionLayoutListConfiguration(appearance: .plain)
            config.headerMode = .supplementary
            return NSCollectionLayoutSection.list(using: config, layoutEnvironment: env)
        }
        let view = UICollectionView(
            frame: .zero,
            collectionViewLayout: layout
        ).prepareForAutoLayout()
        view.backgroundColor = .systemBackground
        return view
    }()
    
    //MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDataSource()
        presenter?.fetchData()
    }
    
    //MARK: Setup
    private func setupUI() {
        view.backgroundColor = .systemBackground
        title = "Mooncascade"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        navigationController?.navigationBar.topItem?.titleView = searchBar
        searchBar.delegate = self
        
        view.addSubview(segmentedControl)
        segmentedControl.topAnchor ~= view.safeAreaLayoutGuide.topAnchor + 8
        segmentedControl.leadingAnchor ~= view.safeAreaLayoutGuide.leadingAnchor + 16
        segmentedControl.trailingAnchor ~= view.safeAreaLayoutGuide.trailingAnchor - 16
        segmentedControl.addAction(
            UIAction { [weak self] _ in
                self?.sourceWasChanged()
            },
            for: .valueChanged
        )
        
        view.addSubview(collectionView)
        collectionView.topAnchor ~= segmentedControl.bottomAnchor + 8
        collectionView.leadingAnchor ~= view.safeAreaLayoutGuide.leadingAnchor
        collectionView.trailingAnchor ~= view.safeAreaLayoutGuide.trailingAnchor
        collectionView.bottomAnchor ~= view.safeAreaLayoutGuide.bottomAnchor
        collectionView.delegate = self
        
        refreshControll.addAction(
            UIAction { [weak self] _ in
                self?.presenter?.fetchData()
            },
            for: .valueChanged
        )
        collectionView.refreshControl = refreshControll
        
        view.addSubview(loader)
        loader.pinCenterToSuperviewCenter()
        showLoader(true)
        
        view.addSubview(updateLabelView)
        updateLabelView.topAnchor ~= segmentedControl.bottomAnchor + 16
        updateLabelView.centerXAnchor ~= view.centerXAnchor
    }

    private func setupDataSource() {
        let cellRegistration = UICollectionView.CellRegistration<EmployeesListCell, CellModel> { cell, indexPath, cellModel in
            cell.model = cellModel
            cell.didTapContactButton = { [weak self] in
                guard let self = self else { return }
                self.router?.routeToContact(by: indexPath)
            }
        }
        
        let headerRegistration = UICollectionView.SupplementaryRegistration<EmployeesListHeaderView>(elementKind: UICollectionView.elementKindSectionHeader) { [weak self] supplementaryView, elementKind, indexPath in
            guard
                let self = self,
                let snapshot = self.dataSource?.snapshot()
            else { return }
            let section = snapshot.sectionIdentifiers[indexPath.section]
            supplementaryView.title = section.title
        }
        
        dataSource = UICollectionViewDiffableDataSource<SectionModel, CellModel>(collectionView: collectionView, cellProvider: { collectionView, indexPath, itemIdentifier in
            return collectionView.dequeueConfiguredReusableCell(using: cellRegistration, for: indexPath, item: itemIdentifier)
        })
        dataSource?.supplementaryViewProvider = { (collectionView, kind, indexPath) in
            return collectionView.dequeueConfiguredReusableSupplementary(using: headerRegistration, for: indexPath)
        }
    }
}

//MARK: Private
private extension EmployeesListViewController {
    private func sourceWasChanged() {
        let index = segmentedControl.selectedSegmentIndex
        guard let source = SourceType(rawValue: index) else { return }
        showLoader(true)
        presenter?.sourceWasChanged(source)
    }
    
    private func showLoader(_ show: Bool) {
        if show {
            loader.startAnimating()
        } else {
            loader.stopAnimating()
        }
        view.isUserInteractionEnabled = !show
    }
}

//MARK: View Logic

extension EmployeesListViewController: EmployeesListViewLogic {
    func applyViewModel(_ viewModel: EmployeesListScreen.ViewModel) {
        showLoader(false)
        if !refreshControll.isRefreshing {
            updateLabelView.isHidden = !viewModel.isCachedData
        } else {
            if !viewModel.isCachedData {
                refreshControll.endRefreshing()
            }
        }
        var snapshot = NSDiffableDataSourceSnapshot<SectionModel, CellModel>()
        viewModel.sectionViewModels.forEach {
            snapshot.appendSections([$0])
            snapshot.appendItems($0.cellViewModels, toSection: $0)
        }
        dataSource?.apply(snapshot)
    }
    
    func displayError() {
        refreshControll.endRefreshing()
        router?.displayError()
    }
}

//MARK: Navigation

extension EmployeesListViewController: NavigationLogic {
    func push(_ viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func present(_ viewController: UIViewController) {
        navigationController?.present(viewController, animated: true, completion: nil)
    }
}

//MARK: CollectionView Delegate
extension EmployeesListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        router?.routeToDetails(by: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
}

//MARK: SearchBar Delegate
extension EmployeesListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter?.search(searchText: searchText)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
