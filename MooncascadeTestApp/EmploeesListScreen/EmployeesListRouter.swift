//
//  EmployeesListRouter.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 03.10.2021.
//

import Foundation
import ContactsUI

protocol EmployeesListRoutingLogic: AnyObject {
    func routeToDetails(by indexPath: IndexPath)
    func routeToContact(by indexPath: IndexPath)
    func displayError()
}

final class EmployeesListRouter: EmployeesListRoutingLogic {
    private weak var navigation: NavigationLogic?
    private let dataStore: EmployeesListDataStore
    init(
        navigation: NavigationLogic,
        dataStore: EmployeesListDataStore
    ) {
        self.navigation = navigation
        self.dataStore = dataStore
    }
    
    func routeToDetails(by indexPath: IndexPath) {
        guard let employee = dataStore.getEmployee(indexPath) else { return }
        let controller = EmployeeDetailsScreen.assemble(employee: employee)
        navigation?.push(controller)
    }
    
    func routeToContact(by indexPath: IndexPath) {
        guard let contact = dataStore.getContact(indexPath) else { return }
        let contactController = CNContactViewController(for: contact)
        contactController.allowsEditing = false
        navigation?.push(contactController)
    }
    
    func displayError() {
        let alert = UIAlertController(title: "Something wrong", message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(action)
        navigation?.present(alert)
    }
}
