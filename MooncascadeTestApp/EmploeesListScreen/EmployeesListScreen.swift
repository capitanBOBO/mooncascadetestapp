//
//  EmployeesListScreen.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 03.10.2021.
//

import Foundation
import UIKit

enum EmployeesListScreen {
    
    struct SectionViewModel: Hashable {
        let title: String
        let cellViewModels: [EmployeesListCell.Model]
        private let identifier = UUID().uuidString
    }
    
    struct ViewModel {
        let sectionViewModels: [SectionViewModel]
        let isCachedData: Bool
    }
    
    static func assemble() -> UIViewController {
        let viewController = EmployeesListViewController()
        let client = App.app.defaultClient
        let service = EmployeeService(client: client)
        let presenter = EmployeesListPresenter(
            sourceType: .tallin,
            view: viewController,
            service: service,
            contactsService: ContactsService.shared
        )
        let router = EmployeesListRouter(
            navigation: viewController,
            dataStore: presenter
        )
        viewController.presenter = presenter
        viewController.router = router
        return viewController
    }
}
