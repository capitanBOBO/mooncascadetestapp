//
//  EmployeesListHeaderView.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 09.10.2021.
//

import UIKit

final class EmployeesListHeaderView: UICollectionReusableView {
    private let titleLabel = UILabel().prepareForAutoLayout()
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("Not implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
    }

    private func setupUI() {
        backgroundColor = .systemBackground
        addSubview(titleLabel)
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        titleLabel.pinEdgesToSuperviewEdges(top: 8, left: 16, bottom: 8)
    }
}
