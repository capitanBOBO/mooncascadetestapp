//
//  EmployeesListCell.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 18.10.2021.
//

import UIKit

final class EmployeesListCell: UICollectionViewCell {
    struct Model: Hashable {
        let title: String
        let showContactButton: Bool
        private let identifier = UUID().uuidString
    }
    private let listContentView = UIListContentView(configuration: .cell()).prepareForAutoLayout()
    private let contactButton = UIButton(type: .detailDisclosure).prepareForAutoLayout()
    var didTapContactButton: (()->())?
    var model: Model? {
        didSet {
            guard let model = model else { return }
            contactButton.isHidden = !model.showContactButton
            var content: UIListContentConfiguration = .cell()
            content.text = model.title
            listContentView.configuration = content
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not implemented")
    }
    
    private func setupUI() {
        addSubview(listContentView)
        
        addSubview(contactButton)
        contactButton.trailingAnchor ~= trailingAnchor
        contactButton.heightAnchor ~= 45
        contactButton.widthAnchor ~= 45
        contactButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        contactButton.addAction(
            UIAction { [weak self] _ in
                self?.didTapContactButton?()
            },
            for: .touchUpInside
        )
    }
}
