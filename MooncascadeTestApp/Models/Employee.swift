//
//  Employee.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 03.10.2021.
//

import Foundation

struct EmploeeContactDetails: Codable {
    let email: String
    let phone: String?
}

struct Employee: Codable {
    enum Position {
        case ios
        case android
        case web
        case pm
        case tester
        case sales
        case other
    }
    let firstName: String
    let lastName: String
    let contactDetails: EmploeeContactDetails
    let position: String
    let projects: [String]?
    
    enum CodingKeys: String, CodingKey {
        case firstName = "fname"
        case lastName = "lname"
        case contactDetails = "contact_details"
        case position
        case projects
    }
}
