//
//  EmployeeDetailsScreen.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 10.10.2021.
//

import Foundation
import UIKit

enum EmployeeDetailsScreen {
    struct ViewModel {
        let fullName: String
        let email: String
        let phone: String
        let position: String
        let projects: [String]
        let hasContact: Bool
    }
    
    static func assemble(employee: Employee) -> EmployeeDetailsViewController {
        let viewController = EmployeeDetailsViewController()
        let presenter = EmployeeDetailsPresenter(
            view: viewController,
            contactsService: ContactsService.shared,
            employee: employee
        )
        let router = EmployeeDetailsRouter(
            navigation: viewController,
            dataStore: presenter
        )
        viewController.presenter = presenter
        viewController.router = router
        return viewController
    }
}
