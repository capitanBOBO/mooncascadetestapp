//
//  EmployeeDetailsPresenter.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 10.10.2021.
//

import Foundation
import Contacts

protocol EmployeeDetailsBusinessLogic: AnyObject {
    func fetchData()
}

protocol EmployeeDetailsDataStore: AnyObject {
    func getContact() -> CNContact?
}

class EmployeeDetailsPresenter: EmployeeDetailsBusinessLogic, EmployeeDetailsDataStore {
    private var employee: Employee
    private weak var view: EmployeeDetailsViewLogic?
    private var contactsService: ContactsServiceLogic
    init(view: EmployeeDetailsViewLogic,
         contactsService: ContactsServiceLogic,
         employee: Employee) {
        self.view = view
        self.contactsService = contactsService
        self.employee = employee
    }
    
    func fetchData() {
        switch contactsService.status {
        case .unknown:
            contactsService.authorize { [weak self] result in
                guard let self = self else { return }
                if case .success(let status) = result, status == .authorized {
                    if self.contactsService.contacts.isEmpty {
                        self.contactsService.fetchContacts()
                    }
                }
            }
        case .authorized:
            if contactsService.contacts.isEmpty {
                contactsService.fetchContacts()
            }
        default:
            break
        }
        
        view?.applyViewModel(prepareViewModel())
    }
    
    func getContact() -> CNContact? {
        let name = "\(employee.firstName) \(employee.lastName)".lowercased()
        let contact = contactsService.contacts.first(where: {
            $0.givenName.lowercased() == name ||
            "\($0.givenName) \($0.familyName)".lowercased() == name
        })
        return contact
    }
    
    private func prepareViewModel() -> EmployeeDetailsScreen.ViewModel {
        let hasContact = getContact() != nil
        return .init(
            fullName: "\(employee.firstName) \(employee.lastName)",
            email: employee.contactDetails.email,
            phone: employee.contactDetails.phone ?? "",
            position: employee.position,
            projects: employee.projects ?? [],
            hasContact: hasContact
        )
    }
}
