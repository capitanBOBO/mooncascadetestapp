//
//  EmployeeDetailsViewController.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 10.10.2021.
//

import UIKit

protocol EmployeeDetailsViewLogic: AnyObject {
    func applyViewModel(_ viewModel: EmployeeDetailsScreen.ViewModel)
}

final class EmployeeDetailsViewController: UIViewController, EmployeeDetailsViewLogic {
    var presenter: EmployeeDetailsBusinessLogic?
    var router: EmployeeDetailsRoutingLogic?
    
    private let mainStackView = UIStackView().prepareForAutoLayout()
    private let buttonContainer = UIView().prepareForAutoLayout()
    private let scrollView = UIScrollView().prepareForAutoLayout()
    private let stackView = UIStackView().prepareForAutoLayout()
    private var contactButton: UIButton = {
        let button = UIButton().prepareForAutoLayout()
        button.setTitle("Show contact", for: .normal)
        button.setTitleColor(.systemBackground, for: .normal)
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 5
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter?.fetchData()
    }
    
    private func setupUI() {
        view.backgroundColor = .systemBackground
        view.addSubview(mainStackView)
        mainStackView.topAnchor ~= view.safeAreaLayoutGuide.topAnchor
        mainStackView.bottomAnchor ~= view.safeAreaLayoutGuide.bottomAnchor
        mainStackView.leadingAnchor ~= view.safeAreaLayoutGuide.leadingAnchor
        mainStackView.trailingAnchor ~= view.safeAreaLayoutGuide.trailingAnchor
        mainStackView.axis = .vertical
        
        buttonContainer.addSubview(contactButton)
        buttonContainer.heightAnchor ~= 60
        
        let action = UIAction { [weak self] _ in
            self?.router?.routeToContact()
        }
        contactButton.addAction(action, for: .touchUpInside)
        contactButton.pinEdgesToSuperviewEdges(top: 8, bottom: 8)
        contactButton.widthAnchor ~= 200
        contactButton.centerXAnchor ~= buttonContainer.centerXAnchor
        
        mainStackView.insertArrangedSubview(buttonContainer, at: 0)
        
        mainStackView.insertArrangedSubview(scrollView, at: 0)
        scrollView.showsVerticalScrollIndicator = false
        
        scrollView.addSubview(stackView)
        stackView.pinEdgesToSuperviewEdges()
        stackView.widthAnchor ~= scrollView.widthAnchor
        stackView.axis = .vertical
    }
    
    func applyViewModel(_ viewModel: EmployeeDetailsScreen.ViewModel) {
        title = viewModel.fullName
        buttonContainer.isHidden = !viewModel.hasContact
        for index in 0..<5 {
            let title: String
            let text: String
            switch index {
            case 0:
                guard !viewModel.projects.isEmpty else { continue }
                title = "Projects"
                text = viewModel.projects.joined(separator: "\n")
            case 1:
                guard !viewModel.position.isEmpty else { continue }
                title = "Position"
                text = viewModel.position
            case 2:
                guard !viewModel.phone.isEmpty else { continue }
                title = "Phone number"
                text = viewModel.phone
            case 3:
                guard !viewModel.email.isEmpty else { continue }
                title = "Email"
                text = viewModel.email
            case 4:
                guard !viewModel.fullName.isEmpty else { continue }
                title = "Full name"
                text = viewModel.fullName
            default:
                return
            }
            let view = DetailCellView().prepareForAutoLayout()
            view.model = .init(title: title, text: text)
            view.heightAnchor >= 45
            stackView.insertArrangedSubview(view, at: 0)
        }
    }
}

extension EmployeeDetailsViewController: NavigationLogic {
    func present(_ viewController: UIViewController) {
        navigationController?.present(viewController, animated: true, completion: nil)
    }
    
    func push(_ viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
}
