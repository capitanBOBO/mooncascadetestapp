//
//  DetailCellView.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 16.10.2021.
//

import UIKit

final class DetailCellView: UIView {
    
    struct Model {
        let title: String
        let text: String
    }

    private let titleLabel = UILabel().prepareForAutoLayout()
    private let textLabel = UILabel().prepareForAutoLayout()
    
    var model: Model? {
        didSet {
            guard let model = model else { return }
            titleLabel.text = model.title
            textLabel.text = model.text
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not implemented")
    }
    
    private func setupUI() {
        addSubview(titleLabel)
        titleLabel.pinEdgesToSuperviewEdges(top: 8, left: 16, right: 8)
        titleLabel.font = UIFont.systemFont(ofSize: 12)
        titleLabel.textColor = .lightGray
        titleLabel.numberOfLines = 1
        
        addSubview(textLabel)
        textLabel.pinEdgesToSuperviewEdges(left: 16, bottom: 8, right: 16)
        textLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 8).isActive = true
        textLabel.numberOfLines = 0
    }
}
