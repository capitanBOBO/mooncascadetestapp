//
//  EmployeeDetailsRouter.swift
//  MooncascadeTestApp
//
//  Created by Иван Савин on 18.10.2021.
//

import UIKit
import ContactsUI

protocol EmployeeDetailsRoutingLogic: AnyObject {
    func routeToContact()
}

final class EmployeeDetailsRouter: EmployeeDetailsRoutingLogic {
    private weak var navigation: NavigationLogic?
    private var dataStore: EmployeeDetailsDataStore
    
    init(
        navigation: NavigationLogic,
        dataStore: EmployeeDetailsDataStore
    ) {
        self.navigation = navigation
        self.dataStore = dataStore
    }
    
    func routeToContact() {
        guard let contact = dataStore.getContact() else { return }
        let contactController = CNContactViewController(for: contact)
        contactController.allowsEditing = false
        navigation?.push(contactController)
    }
}
