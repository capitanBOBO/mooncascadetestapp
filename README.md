# MoonCascadeTestApp

## Task

Download list of employees (two sources), display them, on tap show details. Implement search, caching and contact book (open contact native if it's exist in contact book).

## Before launching

Be shure that you loged in you apple developer account if you want check app on phone. Go to app settings then choose ***Signing&Capabilites*** and set signing on auto and select your account team. If Xcode say "invalid identifiers" then try different one.

Same for target with unit tests

If you use Simulator then you can just make build.

Target iOS setted at 14.7, but it should work on 15.0.

![screenshot](xcode_screenshot.png)

## Architecture

I choosed **MVP + Router** architecture. For this small app **MVVM** with react or **VIPER**-like architectures will be too much. **MVP** will be enough. **M** - model, in this case it's just data. **V** - view, view controller in this case. **P** - presenter, making request via services and preparing data. If it's requiered inf future we can easelly implement another layer for separation presenter at two independet layers.

Maybe for this app any architecture will be litle bit OP, but we need cover our code by tests and standart ***MVC*** will be absolute unfit

### Example

In next example `String` text work as ***Model***. In work code I use `struct` as models and ***ViewModel*** as DTO between ***Presenter*** and ***View*** 

```
// View

protocol ViewLogic: AnyObject {
	func displayText(text: String)
}

protocol NavigationLogic: AnyObjectt {
	func push(_ viewController: UIViewController)
}

class ViewController: UIViewController, ViewLogic, NavigationController {
	var presenter: BusinessLogic?
	var router: RoutingLogic?
	var label: UILabel = UILabel()
	var button: UIButton = UIButton()

	override func viewDidLoad() {
		super.viewDidLoad()
		presenter?.fetchData()
		button.addAction(UIAction{ [weak self] _ in
			self?.router.route()
		}, for: .touchUpInside)
	}

	func displayText(text: String) {
		label.text = text
	}

	func push(_ viewController: UIViewContrtoller) {
		navigationController?.pushViewController(viewController, animated: true)
	}
}


// Presenter

protocol BusinessLogic: AnyObject {
	func fetchData()
}

protocol DataStore: AnyObject {
	func getText() -> String
}

class Presenter: BusinessLogic, DataStore {
	weak var view: ViewLogic?
	var service: ServiceInterface?
	var storedText: String = ""
	func fetchData() {
		service.requestText { [weak self] result
			if case .success(let text) = result {
				self?.storesText = text
				self?.view?.displayText(text: storedText)
			}
		}
	}

	func getText() -> String {
		return storedText
	}
}


// Router

protocol RoutingLogic: AnyObject {
	func route()
}

class Router: RoutingLogic {
	weak var navigation: NavigationLogic?
	var dataStore: DataStore?

	func route() {
		let text = dataStore?.getText() ?? ""
		let controller = UIViewConroller()
		controller.title = text
		navigation?.push(controller)
	}
}


// Assembling

func assemble() -> ViewController {
	let viewController = ViewController()
	let router = Router()
	let presenter = Presenter()
	let client = Client()
	let service = Service()
	service.client = client
	presenter.view = viewController
	presenter.service = service
	router.navigation = viewController
	router.dataStore = presenter
	return viewController
}
```

I described requests in ***Service*** which use ***NetworkClients***, you can easy swap them for watever you want, for example another ***NetworkClient*** which use different source

```
protocol ClientInterface {
	func request(url: String, completion: (Result<String, Error>)->())
}

class Client: ClientInterface {
	func request(url: String, completion: (Result<String, Error>)->()) {
		//URLReqest here
		completion(.success("Hello, World"))
	}
}

protocol ServiceInterfacte {
	func requestText(_ completion: (Result<String, Error>)->())
}

class Service: ServiceInterface {
	var client: ClientInterface?
	func requestText(_ completion: (Result<String, Error>)->()) {
		client?.request("https://hello.world.com") { result
			completion(result)
		}
	}
}
```

## Data caching

Instead of using default `NSCache` I'm prepared different solution which storing requests data via `FileManager` into `cacheDirectory`. For uniq key I use `urlString.utf8`. For current task it's will be enough. I can't use `hashValue` because it's different from launch to launch. I have sha1 encryption algorythm, but unfortunately I not completely understand, how it's work. Just leave it here.

For response data caching I created `CachedNetworkClient` which work with `ClientInterface` protocol and can be swaped by regular `NetworkClient` without cahcing. It's allow us write tests for ***Services*** which has `ClientInterface` dependency.

## Collection view

Of course for simple list we can use simple `UITableView`. But, when Apple anounced their `UICollectionViewCompositionLayout`, it was look like old one `UITableView` should be pass away soon. For that you can use `UICollectionLayoutListConfiguration` and easy make lists. And I really want to try it and this test task is my chanse! 

Not everything so good as I dreamed. Yes, in some cases with new layouts it's easy to make complex collection layout, but it's CAN BE complex for easy one. But still I choosed composition layout for practice reasons.

## Testing

I covered by test as much as I can. All units which not contain out-of-box libraries. For UI always an option. What should be checked:
* Data displaying; 
* Loaders behaviour;
* Transitions;
* Dark theme.

For some logic:
* "Show contact" button appearing;
* Search - logic was tested by unit test, but screen behaviour can be checked;
* Requests - source chainging before loading will be finished;
* Caching - data should be displayed on next launch.
